#!/bin/bash

if [ ! "$(which sassc 2> /dev/null)" ]; then
   echo sassc needs to be installed to generate the css.
   exit 1
fi

if [ ! "$(which git 2> /dev/null)" ]; then
   echo git needs to be installed to check GTK.
   exit 1
fi

SASSC_OPT="-M -t compact"

: ${ELEMENTARY_SOURCE_PATH:="../../../elementary/stylesheet"}

sassc $SASSC_OPT -I${ELEMENTARY_SOURCE_PATH}/src/ \
	elementary.scss elementary.css
sassc $SASSC_OPT -I${ELEMENTARY_SOURCE_PATH}/src/ \
	elementary-dark.scss elementary-dark.css

