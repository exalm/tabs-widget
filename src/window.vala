[GtkTemplate (ui = "/org/example/App/ui/demo-window.ui")]
public class TabsWidget.Window : Hdy.ApplicationWindow {
    [GtkChild]
    private Hdy.TabView view;

    private const ActionEntry[] action_entries = {
        { "tab-new", tab_new },
        { "fullscreen", do_fullscreen },
        { "restore", restore },
    };

    private const ActionEntry[] tab_action_entries = {
        { "pin", tab_pin },
        { "unpin", tab_unpin },
        { "close", tab_close },
        { "close-other", tab_close_other },
        { "close-left", tab_close_left },
        { "close-right", tab_close_right },
        { "move-to-new-window", tab_move_to_new_window },
    };

    private const string[] ICON_NAMES = {
        "text-x-generic-symbolic",
        "web-browser-symbolic",
        "airplane-mode-symbolic",
        "checkbox-symbolic",
        "folder"
    };

    private SimpleActionGroup tab_action_group;
    private Hdy.TabPage? menu_page;
    private static int next_page = 1;

    public bool is_fullscreen { get; internal set; }

    public Window (Gtk.Application app) {
        Object (application: app);

        tab_new ();
        tab_new ();
        tab_new ();
    }

    public Window.from (Window window, bool prepopulate) {
        Object (application: window.application);

        if (prepopulate) {
            tab_new ();
            tab_new ();
            tab_new ();
        }
    }

    construct {
        add_action_entries (action_entries, this);

        tab_action_group = new SimpleActionGroup ();
        tab_action_group.add_action_entries (tab_action_entries, this);
        insert_action_group ("tab", tab_action_group);
    }

    private void tab_new () {
        var title = @"Page $(next_page)";

        var content = new DemoPage ();
        content.show ();

        var page = view.append (content);

        page.title = title;
        page.icon = new ThemedIcon (ICON_NAMES[next_page % ICON_NAMES.length]);

        content.set_page (view, page);

        view.selected_page = page;

        next_page++;
    }

    private void do_fullscreen () {
        is_fullscreen = true;
        fullscreen ();
    }

    private void restore () {
        is_fullscreen = false;
        unfullscreen ();
    }

    private void set_tab_action_enabled (string name, bool enabled) {
        var action = tab_action_group.lookup_action (name);

        assert (action is SimpleAction);

        var simple_action = action as SimpleAction;

        simple_action.set_enabled (enabled);
    }

    [GtkCallback]
    private bool close_page_cb (Hdy.TabPage page) {
        var demo_page = page.content as DemoPage;

        return demo_page.try_close ();
    }

    [GtkCallback]
    private void setup_menu_cb (Hdy.TabPage? page) {
        menu_page = page;

        if (page != null) {
            int pos = view.get_page_position (page);
            var prev = pos > 0 ? view.get_nth_page (pos - 1) : null;

            bool can_close_left = !page.pinned && prev != null && !prev.pinned;
            bool can_close_right = pos < view.n_pages - 1;

            set_tab_action_enabled ("pin", !page.pinned);
            set_tab_action_enabled ("unpin", page.pinned);
            set_tab_action_enabled ("close", !page.pinned);
            set_tab_action_enabled ("close-left", can_close_left);
            set_tab_action_enabled ("close-right", can_close_right);
            set_tab_action_enabled ("close-other", can_close_left || can_close_right);
            set_tab_action_enabled ("move-to-new-window", !page.pinned && view.n_pages > 1);
        } else {
            set_tab_action_enabled ("pin", true);
            set_tab_action_enabled ("unpin", true);
            set_tab_action_enabled ("close", true);
            set_tab_action_enabled ("close-left", true);
            set_tab_action_enabled ("close-right", true);
            set_tab_action_enabled ("close-other", true);
            set_tab_action_enabled ("move-to-new-window", true);
        }
    }

    private void tab_pin () {
        view.set_page_pinned (menu_page, true);
    }

    private void tab_unpin () {
        view.set_page_pinned (menu_page, false);
    }

    private void tab_close () {
        view.close_page (menu_page);
    }

    private void tab_close_other () {
        view.close_other_pages (menu_page);
    }

    private void tab_close_left () {
        view.close_pages_before (menu_page);
    }

    private void tab_close_right () {
        view.close_pages_after (menu_page);
    }

    private void tab_move_to_new_window () {
        var window = new Window.from (this, false);

        view.transfer_page (menu_page, window.view, 0);

        window.present ();
    }

    [GtkCallback]
    private unowned Hdy.TabView? create_window_cb () {
        var window = new Window.from (this, false);
        window.set_position (Gtk.WindowPosition.MOUSE);

        window.present ();

        return window.view;
    }
}
